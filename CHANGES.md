# 0.0.4 - January 25th, 2024

## Addition

- Created a simple context manager to allow writing to temporary files before replacing the target file.

## Fixes

# 0.0.3 - January 25th, 2024

## Fixes

- Metadata: Minimum Python version dropped to 3.8.

# 0.0.2 - January 24th, 2024

Mostly metadata fixes, and a small additional API method.

## Added

- `ue5config.UE5Config.get_or_create_key()` - If a section key does not exist, create it. Then, return either the found or created section.

# 0.0.1 - January 24th, 2024

Initial release
