import collections
from unittest import main, TestCase

from ue5config.uson import USONSerializer

__all__ = ["SerializationTests"]


class SerializationTests(TestCase):
    def setUp(self) -> None:
        self.uson = USONSerializer()

    def test_serialize_ints(self) -> None:
        self.assertEqual(self.uson.serialize_int(-1234), '-1234')
        self.assertEqual(self.uson.serialize_int(-123), '-123')
        self.assertEqual(self.uson.serialize_int(-12), '-12')
        self.assertEqual(self.uson.serialize_int(-1), '-1')
        self.assertEqual(self.uson.serialize_int(0), '0')
        self.assertEqual(self.uson.serialize_int(1), '1')
        self.assertEqual(self.uson.serialize_int(12), '12')
        self.assertEqual(self.uson.serialize_int(123), '123')
        self.assertEqual(self.uson.serialize_int(1234), '1234')
    
        # ints from DefaultPalWorldSettings.ini - 30-01-2024
        self.assertEqual(self.uson.serialize_int(0), '0')
        self.assertEqual(self.uson.serialize_int(4), '4')
        self.assertEqual(self.uson.serialize_int(15), '15')
        self.assertEqual(self.uson.serialize_int(20), '20')
        self.assertEqual(self.uson.serialize_int(32), '32')
        self.assertEqual(self.uson.serialize_int(100), '100')
        self.assertEqual(self.uson.serialize_int(128), '128')
        self.assertEqual(self.uson.serialize_int(3000), '3000')
        self.assertEqual(self.uson.serialize_int(8211), '8211')
        self.assertEqual(self.uson.serialize_int(25575), '25575')
    
    def test_serialize_floats(self) -> None:
        self.assertEqual(self.uson.serialize_float(-1234.5), "-1234.500000")
        self.assertEqual(self.uson.serialize_float(-123.4), "-123.400000")
        self.assertEqual(self.uson.serialize_float(-12.3), "-12.300000")
        self.assertEqual(self.uson.serialize_float(-1.2), "-1.200000")
        self.assertEqual(self.uson.serialize_float(-1.0), "-1.000000")
        self.assertEqual(self.uson.serialize_float(0.0), "0.000000")
        self.assertEqual(self.uson.serialize_float(1.0), "1.000000")
        self.assertEqual(self.uson.serialize_float(1.2), "1.200000")
        self.assertEqual(self.uson.serialize_float(12.3), "12.300000")
        self.assertEqual(self.uson.serialize_float(123.4), "123.400000")
        self.assertEqual(self.uson.serialize_float(1234.5), "1234.500000")
    
        # floats from DefaultPalWorldSettings.ini - 30-01-2024
        self.assertEqual(self.uson.serialize_float(1.0), "1.000000")
        self.assertEqual(self.uson.serialize_float(72.0), "72.000000")
    
    def test_serialize_bools(self) -> None:
        self.assertEqual(self.uson.serialize_bool(True), "True")
        self.assertEqual(self.uson.serialize_bool(False), "False")
    
    def test_serialize_strs(self) -> None:
        self.assertEqual(self.uson.serialize_str(' Test'), '" Test"')
        self.assertEqual(self.uson.serialize_str('$Test'), '$Test')
        self.assertEqual(self.uson.serialize_str('//Test'), '"//Test"')
        self.assertEqual(self.uson.serialize_str('1Test'), '1Test')
        self.assertEqual(self.uson.serialize_str('TEST'), 'TEST')
        self.assertEqual(self.uson.serialize_str('Test'), 'Test')
        self.assertEqual(self.uson.serialize_str('a'), 'a')
        self.assertEqual(self.uson.serialize_str('{Test}'), '"{Test}"')
        
        # strings from DefaultPalWorldSettings.ini - 30-01-2024
        self.assertEqual(self.uson.serialize_str(''), '""')
        self.assertEqual(self.uson.serialize_str('All'), 'All')
        self.assertEqual(self.uson.serialize_str('Default Palworld Server'), '"Default Palworld Server"')
        self.assertEqual(self.uson.serialize_str('https://api.palworldgame.com/api/banlist.txt'), '"https://api.palworldgame.com/api/banlist.txt"')
        
        # First two strs from Engine.ini - 30-01-2024
        self.assertEqual(self.uson.serialize_str('../../../Engine/Content'),'../../../Engine/Content')
        self.assertEqual(self.uson.serialize_str('%GAMEDIR%Content'),'%GAMEDIR%Content')
    
    def test_serialize_dicts(self) -> None:
        self.assertEqual(self.uson.serialize_dict(collections.OrderedDict(a=1, b=2.0, c="3", d=True, e="Five")),'(a=1,b=2.000000,c="3",d=True,e=Five)',)
