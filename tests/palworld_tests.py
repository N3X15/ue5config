from pathlib import Path
from unittest import TestCase, main

from ue5config import UE5Config

__all__ = ["PalWorldTests"]


class PalWorldTests(TestCase):
    def test_palworld_settings(self) -> None:
        """
        This test copies default Palworld dedicated server settings, customizes them,
        and then saves them to MyServerSettings.ini.
        @see <../README.md>
        """

        cfg = UE5Config()
        cfg.read_file((Path("samples") / "palworld" / "DefaultPalWorldSettings.ini"))

        # Dump to JSON
        cfg.write_json(
            Path("samples") / "palworld" / "DefaultPalWorldSettings.json", indent=2
        )

        # Dump to YAML - requires ruamel.yaml package
        cfg.write_yaml(Path("samples") / "palworld" / "DefaultPalWorldSettings.yml")

        # Dump to TOML - requires toml package
        cfg.write_toml(Path("samples") / "palworld" / "DefaultPalWorldSettings.toml")

        ## Mess around

        # Get the main section
        palworld_section = cfg.get_or_create("/Script/Pal.PalGameWorldSettings")

        # Get the option all the settings are crammed into for some reason
        optsettings = palworld_section["OptionSettings"]

        # Change stuff
        optsettings["ServerName"] = "Grugworld"
        optsettings["ServerPlayerMaxNum"] = 64
        optsettings["PlayerStomachDecreaceRate"] = 0.01
        optsettings["ServerDescription"] = "Brought to you by the Eggman Empire"
        # NOTE: If the value is something like All (no quotes), it's just an unquoted string.
        optsettings["DeathPenalty"] = "All"

        ## Saving
        cfg.write_file(Path("samples") / "palworld" / "MyServerSettings.ini")

    def test_palworld_engine(self) -> None:
        """
        This test locks the TPS of the server to 90.
        @see <https://gist.github.com/blackjack4494/628748503c182f5cae04ddacd1e453fa>
        """

        cfg = UE5Config()
        cfg.read_file((Path("samples") / "palworld" / "Engine.ini"))

        # Dump to JSON
        cfg.write_json(Path("samples") / "palworld" / "Engine.json", indent=2)

        # Dump to YAML
        cfg.write_yaml(Path("samples") / "palworld" / "Engine.yml")

        # Dump to TOML
        cfg.write_toml(Path("samples") / "palworld" / "Engine.toml")

        ## Mess around

        # Get the section for /Script/OnlineSubsystemUtils.IpNetDriver
        drv = cfg.get_or_create("/Script/OnlineSubsystemUtils.IpNetDriver")

        # Set tickrate to 90/s
        drv["NetServerMaxTickRate"] = 90

        # Get the Core.System section
        sys = cfg.get_or_create("Core.System")

        # Add an example plugin's content to paths
        sys["Paths"].append("../../../Pal/Plugins/Example/Content")

        ## Saving
        cfg.write_file(Path("samples") / "palworld" / "MyEngine.ini")


if __name__ == "__main__":
    main()
