import os
import tempfile
from pathlib import Path
from types import TracebackType
from typing import IO, Any, Optional, Type

from typing_extensions import Literal

__all__ = ["SaferTemporaryFilename"]


class SaferTemporaryFilename:
    def __init__(self, dir: Path) -> None:
        self.filename: Path = Path(tempfile.NamedTemporaryFile(dir=dir).name)

    def __enter__(self) -> "SaferTemporaryFilename":
        return self

    def __exit__(
        self,
        exc_type: Optional[Type[BaseException]],
        exc_val: Optional[BaseException],
        exc_tb: Optional[TracebackType],
    ) -> Literal[False]:
        if self.filename.is_file():
            self.filename.unlink()
        return False

    def open(
        self,
        mode: str = "r",
        buffering: int = -1,
        encoding: Optional[str] = None,
        errors: Optional[str] = None,
        newline: Optional[str] = None,
    ) -> IO[Any]:
        return self.filename.open(
            mode=mode,
            buffering=buffering,
            encoding=encoding,
            errors=errors,
            newline=newline,
        )

    def replace_other(self, other: Path) -> int:
        os.replace(self.filename, other)
        return os.path.getsize(other)
