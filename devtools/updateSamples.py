import os
import shlex
import shutil
import subprocess
import time
from pathlib import Path
from typing import List

PALWORLD_DEDI_DIR = Path("lib") / "palworld_dedi"
PALWORLD_DEDI_DIR.mkdir(parents=True, exist_ok=True)

SAMPLES_DIR = Path("samples")
PALWORLD_SAMPLES_DIR = SAMPLES_DIR / "palworld"
PALWORLD_SAMPLES_DIR.mkdir(parents=True, exist_ok=True)


def cmd(args: List[str], cwd: Path = Path.cwd()) -> None:
    print("$", f"cwd={cwd}", " ".join([shlex.quote(x) for x in args]))
    subprocess.check_call(args, cwd=cwd)


cmd(
    [
        "steamcmd",
        "+force_install_dir",
        str(PALWORLD_DEDI_DIR.absolute()),
        "+login",
        "anonymous",
        "+app_update",
        "2394010",
        "validate",
        "+quit",
    ],
    cwd=PALWORLD_DEDI_DIR,
)

env = os.environ.copy()
UE_PROJECT_ROOT = PALWORLD_DEDI_DIR.resolve().absolute()
UE_TRUE_SCRIPT_NAME = UE_PROJECT_ROOT / "PalServer.sh"
PALSERVER_BINARY = (
    UE_PROJECT_ROOT / "Pal" / "Binaries" / "Linux" / "PalServer-Linux-Test"
)
env["UE_TRUE_SCRIPT_NAME"] = str(UE_TRUE_SCRIPT_NAME)
env["UE_PROJECT_ROOT"] = str(UE_PROJECT_ROOT)
PALSERVER_BINARY.chmod(0o775)
p = subprocess.Popen([str(PALSERVER_BINARY), "Pal"], env=env)
print("Sleeping 30s while the server starts.")
time.sleep(30)  # 30s
print("Terminating")
p.terminate()
p.wait()

# Copy lib/palworld_dedi/DefaultPalWorldSettings.ini
shutil.copy(
    PALWORLD_DEDI_DIR / "DefaultPalWorldSettings.ini",
    PALWORLD_SAMPLES_DIR / "DefaultPalWorldSettings.ini",
)

# Copy lib/palworld_dedi/Pal/Saved/Config/LinuxServer/Engine.ini
shutil.copy(
    PALWORLD_DEDI_DIR / "Pal" / "Saved" / "Config" / "LinuxServer" / "Engine.ini",
    PALWORLD_SAMPLES_DIR / "Engine.ini",
)
