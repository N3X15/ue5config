import json
import re
from datetime import datetime
from pathlib import Path
from typing import Set

now = datetime.utcnow()
date = f"{now.day:02d}-{now.month:02d}-{now.year:04d}"

SAMPLES_DIR = Path("samples")
PALWORLD_SAMPLES_DIR = SAMPLES_DIR / "palworld"

REG_FLOAT = re.compile(r"\b([+-]?(?:\d+\.\d*|\.\d+))\b")
REG_INT = re.compile(r"\b(\-?[0-9]+(?!\.[0-9]+))\b")
REG_STRINGS = re.compile(r'"([^"]+)"')
REG_STRINGS_QUOTELESS = re.compile(r"(?<==)([A-Za-z_\-][A-Za-z0-9_\-]+)\b")
INT_SAMPLES = (-1234, -123, -12, -1, 0, 1, 12, 123, 1234)
FLOAT_SAMPLES = (-1234.5, -123.4, -12.3, -1.2, -1.0, 0.0, 1.0, 1.2, 12.3, 123.4, 1234.5)
# These require manual updates
STR_SAMPLES = {
    "a": "a",
    "TEST": "TEST",
    "Test": "Test",
    "1Test": "1Test",
    " Test": '" Test"',
    "$Test": "$Test",
    "//Test": '"//Test"',
    "{Test}": '"{Test}"',
}
# These require manual updates
STR_DEFAULT_PALWORLD_SAMPLES = {
    "": '""',
    "All": "All",
    "Default Palworld Server": '"Default Palworld Server"',
    "https://api.palworldgame.com/api/banlist.txt": '"https://api.palworldgame.com/api/banlist.txt"',
}
pw_floats: Set[float] = set()
pw_ints: Set[int] = set()
pw_strings: Set[str] = set()
pw_paths: Set[str] = set()
escaped_paths: Set[str] = set()
with open(PALWORLD_SAMPLES_DIR / "DefaultPalWorldSettings.ini", "r") as f:
    data = f.read()
    for fv in REG_FLOAT.findall(data):
        pw_floats.add(float(fv))
    for iv in REG_INT.findall(data):
        pw_ints.add(int(iv))
    for sv in REG_STRINGS.findall(data):
        pw_strings.add(sv)
    for sv in REG_STRINGS_QUOTELESS.findall(data):
        pw_strings.add(sv)
with open(PALWORLD_SAMPLES_DIR / "Engine.ini", "r") as f:
    for line in f:
        if "=" in line.strip():
            k, v = line.strip().split("=", 1)
            if k == "Paths":
                if v.startswith('"'):
                    v = json.loads(v)
                    escaped_paths.add(v)
                pw_paths.add(v)
                if len(pw_paths) == 2:
                    break
    # print('A',len(pw_paths))
    # print('E',len(escaped_paths))

with open(Path("tests") / "serialization_tests.py", "w") as w:

    def writeline(ln: str) -> None:
        w.write(ln + "\n")

    writeline("import collections")
    writeline("from unittest import main, TestCase")
    writeline("")
    writeline("from ue5config.uson import USONSerializer")
    writeline("")
    writeline('__all__ = ["SerializationTests"]')
    writeline("")
    writeline("")
    writeline("class SerializationTests(TestCase):")
    writeline("    def setUp(self) -> None:")
    writeline("        self.uson = USONSerializer()")
    writeline("")
    writeline(f"    def test_serialize_ints(self) -> None:")
    for i in sorted(INT_SAMPLES):
        writeline(f"        self.assertEqual(self.uson.serialize_int({i}), {str(i)!r})")
    writeline("    ")
    writeline(f"        # ints from DefaultPalWorldSettings.ini - {date}")
    for i in sorted(pw_ints):
        writeline(f"        self.assertEqual(self.uson.serialize_int({i}), {str(i)!r})")
    writeline("    ")
    writeline(f"    def test_serialize_floats(self) -> None:")
    for fv in sorted(FLOAT_SAMPLES):
        writeline(
            f'        self.assertEqual(self.uson.serialize_float({fv!r}), "{fv:0.6f}")'
        )
    writeline("    ")
    writeline(f"        # floats from DefaultPalWorldSettings.ini - {date}")
    for fv in sorted(pw_floats):
        writeline(
            f'        self.assertEqual(self.uson.serialize_float({fv!r}), "{fv:0.6f}")'
        )
    writeline("    ")
    writeline("    def test_serialize_bools(self) -> None:")
    writeline('        self.assertEqual(self.uson.serialize_bool(True), "True")')
    writeline('        self.assertEqual(self.uson.serialize_bool(False), "False")')
    writeline("    ")
    writeline("    def test_serialize_strs(self) -> None:")
    for s, o in sorted(STR_SAMPLES.items()):
        writeline(f"        self.assertEqual(self.uson.serialize_str({s!r}), {o!r})")
    writeline("        ")
    writeline(f"        # strings from DefaultPalWorldSettings.ini - {date}")
    for s, o in sorted(STR_DEFAULT_PALWORLD_SAMPLES.items()):
        writeline(f"        self.assertEqual(self.uson.serialize_str({s!r}), {o!r})")

    writeline("        ")
    writeline(f"        # First two strs from Engine.ini - {date}")
    # None of these should be quoted.
    for path in pw_paths:
        epath = path if path not in escaped_paths else json.dumps(path)
        writeline(
            f"        self.assertEqual(self.uson.serialize_str({path!r}),{epath!r})"
        )

    writeline("    ")
    writeline("    def test_serialize_dicts(self) -> None:")
    writeline(
        '        self.assertEqual(self.uson.serialize_dict(collections.OrderedDict(a=1, b=2.0, c="3", d=True, e="Five")),\'(a=1,b=2.000000,c="3",d=True,e=Five)\',)'
    )
